var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var handle = {}
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/register"] = requestHandlers.register;
handle["/show"] = requestHandlers.show;
handle["/notify"] = requestHandlers.notify;
server.start(router.route, handle);
